package com.example.retrofitandroid.activity;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import com.example.retrofitandroid.BaiLarnService;
import com.example.retrofitandroid.R;
import com.example.retrofitandroid.R.id;
import com.example.retrofitandroid.R.layout;
import com.example.retrofitandroid.R.menu;
import com.example.retrofitandroid.R.string;
import com.example.retrofitandroid.request.TokenRenewalRequest;
import com.example.retrofitandroid.request.UserLoginRequest;
import com.example.retrofitandroid.response.Token;
import com.google.gson.Gson;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends ActionBarActivity {
	private TextView responseTV;
	private EditText usernameEditText;
	private EditText passwordEditText;

	private Token userToken;
	private RestAdapter restAdapter;
	private BaiLarnService service;

	private String api_endpoint;
	private String client_id;
	private String client_secret;
	private String preferences;

	private SharedPreferences sharedPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		usernameEditText = (EditText) findViewById(R.id.usernameEditText);
		passwordEditText = (EditText) findViewById(R.id.passwordEditText);

		api_endpoint = getString(R.string.api_endpoint);
		client_id = getString(R.string.client_id);
		client_secret = getString(R.string.client_secret);
		preferences = getString(R.string.preferences);

		//OkHttp Cache Setup
		OkHttpClient okHttpClient = new OkHttpClient();
		File cacheDir = getBaseContext().getCacheDir();
		Cache cache = null;
		try {
			cache = new Cache(cacheDir, 1024);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		okHttpClient.setCache(cache);
		////////////////////
		
		//Rest Client Setup
		restAdapter = new RestAdapter.Builder()
				.setClient(new OkClient(okHttpClient))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setEndpoint(api_endpoint).build();
		service = restAdapter.create(BaiLarnService.class);
		///////////////////
		
		//Shared Preferences Setup
		sharedPreferences = getSharedPreferences(preferences,
				Context.MODE_PRIVATE);
		////////////////
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void login(View view) {
		String username = usernameEditText.getText().toString();
		String password = passwordEditText.getText().toString();
		UserLoginRequest credential = new UserLoginRequest(username, password,
				client_id, client_secret);
		service.login(credential, new Callback<Token>() {

			@Override
			public void failure(RetrofitError arg0) {
				Toast.makeText(getApplicationContext(), "Fail",
						Toast.LENGTH_LONG).show();
			}

			@Override
			public void success(Token token, Response arg1) {
				userToken = token;
				saveToken(token);

				Toast.makeText(getApplicationContext(), "Login Success",
						Toast.LENGTH_LONG).show();
				finish();
			}
		});
	}

	public void renewToken() {
		try {
			String refreshToken = userToken.getRefresh_token();
			TokenRenewalRequest request = new TokenRenewalRequest(refreshToken,
					client_id, client_secret);
			service.renewToken(request, new Callback<Token>() {

				@Override
				public void failure(RetrofitError err) {
					Toast.makeText(getApplicationContext(),
							"Token Renewal Failed : " + err.getMessage(),
							Toast.LENGTH_LONG).show();
				}

				@Override
				public void success(Token token, Response arg1) {
					userToken = token;
					saveToken(token);
					Toast.makeText(getApplicationContext(),
							"Token Renewal Success", Toast.LENGTH_LONG).show();
				}

			});
		} catch (NullPointerException e) {
			Toast.makeText(getApplicationContext(),
					"Token not found, please login first", Toast.LENGTH_LONG)
					.show();
		}
	}
	
	private void saveToken(Token token)
	{
		Editor prefsEditor = sharedPreferences.edit();
		Gson gson = new Gson();
		String tokenJson = gson.toJson(token);
		prefsEditor.putString("userToken", tokenJson);
		prefsEditor.commit();
	
	}
}
