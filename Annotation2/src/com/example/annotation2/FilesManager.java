package com.example.annotation2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

/*
 * This class will read/write files from device sdcard 
 * Folder Structure
 * Bailarn->Materials->Material Unique Name->PageNo
 */
public class FilesManager {
    // SDCard Path
    private final String APP_PATH = Environment.getExternalStorageDirectory().getPath()+ "/Bailarn";
    private final String MAT_PATH = APP_PATH + "/Materials";
    private String currentPath = ""; //update every time user changes page
    
    // Audio
    private static final String AUDIO_FILE_PREFIX = "AUD_";
	private static final String AUDIO_FILE_SUFFIX = ".3gp"; //Related to AUDIO_OUTPUT_FORMAT in MainActivity
  
    //Image
    private static final String IMG_FILE_PREFIX = "IMG_";
	private static final String IMG_FILE_SUFFIX = ".jpg";
    
    
    public FilesManager(){// Constructor
    	//songsList = new ArrayList<HashMap<String, String>>();
    	//imgList = new ArrayList<HashMap<String, String>>();
    }
    
    public void setCurrentPage(int page){
    	currentPath = MAT_PATH + "/" + MainActivity.materialName + "/" + page;
    }
    
    /**
     * Class to filter files which are having .3gp extension
     * */
    class AudioExtensionFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".3gp") );
        }
    }
    class TextExtensionFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".txt"));
        }
    }
    class ImgExtensionFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            return (name.endsWith(".jpg") || name.endsWith(".png") ); //allow reading .png file
        }
    }
 
    //================================== Audio ==============================
    /**
     * Function to read all 3gp files of a specified page of a particular material
     * and store the details in ArrayList
     * */
    public ArrayList<HashMap<String, String>> getPlayList(){
        File home = new File(currentPath);
        ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
        if (home.exists() && home.listFiles(new AudioExtensionFilter()).length > 0) {
            for (File file : home.listFiles(new AudioExtensionFilter())) {
                HashMap<String, String> song = new HashMap<String, String>();
                song.put("songTitle", file.getName().substring(0, (file.getName().length() - AUDIO_FILE_SUFFIX.length() )));
                song.put("songPath", file.getPath());
 
                // Adding each song to SongList
                songsList.add(song);
            }
        }
        // return songs list array
        return songsList;
    }
    
    public String getAudioFilename() {
		File file = new File(currentPath);
		if (!file.exists()) {
			file.mkdirs();
		}
		//return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_OUTPUT_EXT);
		//int newSize = songsList.size() + 1;
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		return (file.getAbsolutePath() + "/" + AUDIO_FILE_PREFIX + timeStamp + AUDIO_FILE_SUFFIX);
	}
    
    //====================== Image ====================
    public ArrayList<HashMap<String, String>> getImgList(){
    	File home = new File(currentPath);
    	ArrayList<HashMap<String, String>> imgList = new ArrayList<HashMap<String, String>>();
        if (home.exists() && home.listFiles(new ImgExtensionFilter()).length > 0) {
            for (File file : home.listFiles(new ImgExtensionFilter())) {
                HashMap<String, String> song = new HashMap<String, String>();
                song.put("imgTitle", file.getName().substring(0, (file.getName().length() - IMG_FILE_SUFFIX.length() )));
                song.put("imgPath", file.getPath());
                imgList.add(song);
            }
        }
        return imgList;
    }
    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //String imageFileName = IMG_FILE_PREFIX + timeStamp;
    	//int newSize = imgList.size() + 1;
    	String imageFileName = IMG_FILE_PREFIX + timeStamp + IMG_FILE_SUFFIX;
        File home = new File(currentPath);
        File image = new File(home, imageFileName);
        /*File image = File.createTempFile(
            imageFileName,  		 prefix 
            IMG_FILE_SUFFIX,          suffix 
            home					 directory 
         );*/
        // Save a file: path for use with ACTION_VIEW intents
        //String mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }
    
    //================================ Text =============================
    public boolean writeTextFile(String text){
    	if(isExternalStorageWritable()){
    		try{
    			String filename = currentPath + "/Note.txt";
                FileOutputStream out = new FileOutputStream(new File(filename));
                out.write(text.getBytes());
                out.close();  
             } catch (Exception e) { 
                e.printStackTrace();
             }
    		return true;
    	}
    	return false;
    }
    
    public String getText(){
    	if(!isExternalStorageReadable() ){
    		return "Sorry. The storage is currently not readable";
    	}
    	else{
    		File home = new File(currentPath);
    		if (home.exists() && home.listFiles(new TextExtensionFilter()).length > 0) {
    			File files[] = home.listFiles(new TextExtensionFilter());
				//Read text from file
				StringBuilder text = new StringBuilder();
				try {
				    BufferedReader br = new BufferedReader(new FileReader(files[0])); //we have only one text file per page folder
				    String line;
				    while ((line = br.readLine()) != null) {
				        text.append(line);
				        text.append('\n');
				    }
				}
				catch (IOException e) {
				    //You'll need to add proper error handling here
				}
				return text.toString();
    		}
    		return ""; //no text file to read
    	}

    }
    
    
    //=============================================================
    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
            Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
}
