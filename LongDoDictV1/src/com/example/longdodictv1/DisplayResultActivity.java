package com.example.longdodictv1;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

public class DisplayResultActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_result);

		Intent intent = getIntent();
		String searchTerm = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
		
		final WebView wv = (WebView)findViewById(R.id.resultTrans);
//		Toast.makeText(MainActivity.this, inputText,Toast.LENGTH_LONG).show();
		wv.loadUrl("http://dict.longdo.com/mobile.php?search="+searchTerm);
	}

}
