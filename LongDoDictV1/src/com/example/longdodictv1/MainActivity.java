package com.example.longdodictv1;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;

public class MainActivity extends ActionBarActivity {

    protected static final String EXTRA_MESSAGE = "com.example.longdodictv1.MESSAGE";

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        final EditText input = (EditText)findViewById(R.id.searchWord);
//        final WebView wv = (WebView)findViewById(R.id.resultTrans);
        input.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if(keyCode == 66){
					String inputText = input.getText().toString();
//					Toast.makeText(MainActivity.this, inputText,Toast.LENGTH_LONG).show();
//					wv.loadUrl("http://dict.longdo.com/mobile.php?search="+inputText);
//					input.setText("");
					
					Intent intent = new Intent(MainActivity.this, DisplayResultActivity.class);
					intent.putExtra(EXTRA_MESSAGE, inputText);
					startActivity(intent);
				}
				return false;
			}
		});
        
    }
}
