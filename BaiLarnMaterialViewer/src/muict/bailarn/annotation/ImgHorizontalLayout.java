package muict.bailarn.annotation;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ImgHorizontalLayout extends LinearLayout {
	
	final int IMG_WIDTH = 220;
	final int IMG_HEIGHT = 220;
	
	final int padding_left = 3;
	final int padding_right = 3;
	final int padding_bottom = 3;
	final int padding_top = 3;
	
	Context myContext;
	ArrayList<String> itemList = new ArrayList<String>();

	public ImgHorizontalLayout(Context context) {
		super(context);
		myContext = context;
	}

	public ImgHorizontalLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		myContext = context;
	}

	@SuppressLint("NewApi") public ImgHorizontalLayout(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		myContext = context;
	}
	
	public void add(String path){
		int newIdx = itemList.size();
		itemList.add(path);
		addView(getImageView(newIdx));
	}
	
	public void clear(){
		removeAllViews();
	}
	
	ImageView getImageView(final int i){
		Bitmap bm = null;
		if (i < itemList.size()){
			bm = decodeSampledBitmapFromUri(itemList.get(i), IMG_WIDTH, IMG_HEIGHT);
		}
		
		ImageView imageView = new ImageView(myContext);
    	imageView.setLayoutParams(new LayoutParams(IMG_WIDTH, IMG_HEIGHT));
    	imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    	imageView.setImageBitmap(bm);
    	imageView.setPadding(padding_left, padding_top, padding_right, padding_bottom);
    	
    	imageView.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getContext(), FullScreenImage.class);
				intent.putExtra("imgPath", itemList.get(i));
				((Activity)getContext()).startActivity(intent);
			}});

		return imageView;
	}
	
	public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
    	Bitmap bm = null;
    	
    	// First decode with inJustDecodeBounds=true to check dimensions
    	final BitmapFactory.Options options = new BitmapFactory.Options();
    	options.inJustDecodeBounds = true;
    	BitmapFactory.decodeFile(path, options);
    	
    	// Calculate inSampleSize
    	options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
    	
    	// Decode bitmap with inSampleSize set
    	options.inJustDecodeBounds = false;
    	bm = BitmapFactory.decodeFile(path, options); 
    	
    	return bm; 	
    }
    
    public int calculateInSampleSize(
    		
    	BitmapFactory.Options options, int reqWidth, int reqHeight) {
    	// Raw height and width of image
    	final int height = options.outHeight;
    	final int width = options.outWidth;
    	int inSampleSize = 1;
        
    	if (height > reqHeight || width > reqWidth) {
    		if (width > height) {
    			inSampleSize = Math.round((float)height / (float)reqHeight);  	
    		} else {
    			inSampleSize = Math.round((float)width / (float)reqWidth);  	
    		}  	
    	}
    	
    	return inSampleSize;  	
    }
	
}

