package muict.bailarn.activity;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import com.example.retrofitandroid.BaiLarnService;
import com.example.retrofitandroid.DateDeserializer;

import muict.bailarn.R;

import com.example.retrofitandroid.request.UserUsageRequest;
import com.example.retrofitandroid.response.Course;
import com.example.retrofitandroid.response.Material;
import com.example.retrofitandroid.response.Token;
import com.example.retrofitandroid.response.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CourseInfoActivity extends Activity {

	private TextView courseInfoTextView;
	private TextView courseCodeTextView;
	private ListView materialListView;

	private Token userToken;
	private RestAdapter restAdapter;
	private BaiLarnService service;

	private String api_endpoint;
	private String client_id;
	private String client_secret;
	private String preferences;

	private SharedPreferences sharedPreferences;

	private User userProfile;
	private Course courseInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course_info);
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// Initialization
		courseInfoTextView = (TextView) findViewById(R.id.classInfoTextView);
		courseCodeTextView = (TextView) findViewById(R.id.courseCodeTextView);
		materialListView = (ListView) findViewById(R.id.materialListView);

		api_endpoint = getString(R.string.api_endpoint);
		client_id = getString(R.string.client_id);
		client_secret = getString(R.string.client_secret);
		preferences = getString(R.string.preferences);
		// OkHttp Cache Setup
		OkHttpClient okHttpClient = new OkHttpClient();
		File cacheDir = getBaseContext().getCacheDir();
		Cache cache = null;
		try {
			cache = new Cache(cacheDir, 1024);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		okHttpClient.setCache(cache);
		// //////////////////
		// Rest Client Setup
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,
				new DateDeserializer()).create();

		restAdapter = new RestAdapter.Builder()
				.setClient(new OkClient(okHttpClient))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setConverter(new GsonConverter(gson))
				.setEndpoint(api_endpoint).build();
		service = restAdapter.create(BaiLarnService.class);
		// /////////////////
		// Shared Preferences Setup
		sharedPreferences = getSharedPreferences(preferences,
				Context.MODE_PRIVATE);
		// //////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////

		// Check for existing token
		if (sharedPreferences.contains("userToken")) {
			gson = new Gson();
			String json = sharedPreferences.getString("userToken", "");
			userToken = gson.fromJson(json, Token.class);
		}
		// Check for existing userProfile, then display it
		if (sharedPreferences.contains("userProfile")) {
			gson = new Gson();
			String json = sharedPreferences.getString("userProfile", "");
			userProfile = gson.fromJson(json, User.class);
		}
		// //////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////
		// ////////////////////////////////////////////////////

		String courseJson = getIntent().getStringExtra("Course");
		gson = new Gson();
		courseInfo = gson.fromJson(courseJson, Course.class);
		displayCourseInfo(courseInfo);

		if (sharedPreferences.contains(courseInfo.getId())) {
			gson = new Gson();
			String json = sharedPreferences.getString(courseInfo.getId(), "");
			Type listType = new TypeToken<List<Material>>() {
			}.getType();
			List<Material> materials = new Gson().fromJson(json, listType);
			displayMaterials(materials);
		}
		getMaterials();
	}

	private void displayCourseInfo(Course courseInfo) {
		courseInfoTextView.setText(courseInfo.toString());
		courseCodeTextView.setText(courseInfo.getCourse_code());
	}

	private void getMaterials() {
		service.getMaterialsFromCourse(userToken.getAccess_token(),
				courseInfo.getId(), new Callback<List<Material>>() {

					@Override
					public void failure(RetrofitError arg0) {
						/*
						 * Toast.makeText(getApplicationContext(),
						 * "Material Retrieval Failed"+ arg0.getMessage(),
						 * Toast.LENGTH_SHORT) .show();
						 */
					}

					@Override
					public void success(List<Material> materials, Response arg1) {
						/*
						 * Toast.makeText(getApplicationContext(),
						 * "Material Retrieval Success", Toast.LENGTH_LONG)
						 * .show();
						 */
						displayMaterials(materials);

						Editor prefsEditor = sharedPreferences.edit();
						Gson gson = new Gson();
						String json = gson.toJson(materials);
						prefsEditor.putString(courseInfo.getId(), json);
						prefsEditor.commit();
					}

				});
	}

	@SuppressLint("NewApi")
	public void displayMaterials(final List<Material> materials) {
		LinearLayout layout = (LinearLayout) findViewById(R.id.materialLayout);
		layout.removeAllViews();
		ListAdapter adapter = new ArrayAdapter<Material>(this,
				android.R.layout.simple_list_item_1, materials); // Your
																	// adapter.
		final int adapterCount = adapter.getCount();
		for (int i = 0; i < adapterCount; i++) {
			final View item = adapter.getView(i, null, null);
			final int position = i;
			LinearLayout matlayout = new LinearLayout(getBaseContext());
			matlayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.grey_box));
			ImageView mIcon = new ImageView(this);
			mIcon.setBackgroundResource(R.drawable.ic_pdf);
			matlayout.setGravity(Gravity.CENTER_VERTICAL);
			matlayout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Material selectedMaterial = materials.get(position);
					UserUsageRequest request = new UserUsageRequest(
							userProfile.getId(), selectedMaterial.getId());
					service.accessMaterial(userToken.getAccess_token(), request, new Callback<Response>() {

						@Override
						public void failure(RetrofitError arg0) {
						}

						@Override
						public void success(Response arg0, Response arg1) {
						}
					});
					Intent intent = new Intent(getApplicationContext(),
							MaterialViewer.class);
					intent.putExtra("course", courseInfo);
					intent.putExtra("material", selectedMaterial);
					startActivity(intent);
				}
			});
			// ------------------------------------------------------------------------------------------------------------------------
			LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			layoutParam.setMargins(0, 0, 0, 50);
			matlayout.setPadding(15, 15, 15, 15);
			matlayout.addView(mIcon);
			matlayout.addView(item);
			layout.addView(matlayout, layoutParam);
			// ------------------------------------------------------------------------------------------------------------------------
		}
	}
}
