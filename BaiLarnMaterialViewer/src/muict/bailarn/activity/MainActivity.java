package muict.bailarn.activity;

import java.io.File;
import java.io.IOException;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

import com.example.retrofitandroid.BaiLarnService;

import muict.bailarn.R;
import com.example.retrofitandroid.response.Token;
import com.example.retrofitandroid.response.User;
import com.google.gson.Gson;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

public class MainActivity extends Activity {

	private Token userToken;
	private RestAdapter restAdapter;
	private BaiLarnService service;

	private String api_endpoint;
	private String client_id;
	private String client_secret;
	private String preferences;

	private SharedPreferences sharedPreferences;

	private User userProfile;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		api_endpoint = getString(R.string.api_endpoint);
		client_id = getString(R.string.client_id);
		client_secret = getString(R.string.client_secret);
		preferences = getString(R.string.preferences);

		// OkHttp Cache Setup
		OkHttpClient okHttpClient = new OkHttpClient();
		File cacheDir = getBaseContext().getCacheDir();
		Cache cache = null;
		try {
			cache = new Cache(cacheDir, 1024);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		okHttpClient.setCache(cache);
		// //////////////////

		// Rest Client Setup
		restAdapter = new RestAdapter.Builder()
				.setClient(new OkClient(okHttpClient))
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setEndpoint(api_endpoint).build();
		service = restAdapter.create(BaiLarnService.class);
		// /////////////////

		// Shared Preferences Setup
		sharedPreferences = getSharedPreferences(preferences,
				Context.MODE_PRIVATE);
		// //////////////

	}

	private boolean resumeHasRun = false;

	protected void onResume() {
		super.onResume();
		// Check for existing token
		if (!sharedPreferences.contains("userToken") & !resumeHasRun) {
			// Go to login page
			resumeHasRun = true;
			Intent myIntent = new Intent(MainActivity.this, LoginActivity.class);
			MainActivity.this.startActivity(myIntent);

		} else if (sharedPreferences.contains("userToken")) {
			Gson gson = new Gson();
			String json = sharedPreferences.getString("userToken", "");
			userToken = gson.fromJson(json, Token.class);

			// Go to user profile page
			Intent myIntent = new Intent(MainActivity.this,
					UserProfileActivity.class);
			MainActivity.this.startActivity(myIntent);
			finish();
		} else {
			finish();
		}
		// //////////////////////
	}
}
