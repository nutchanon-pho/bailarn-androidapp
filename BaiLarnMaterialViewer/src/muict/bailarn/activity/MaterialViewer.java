package muict.bailarn.activity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import android.R.color;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import muict.bailarn.R;
import muict.bailarn.annotation.*;

import com.artifex.mupdf.MuPDFCore;
import com.artifex.mupdf.MuPDFPageAdapter;
import com.artifex.mupdf.view.DocumentReaderView;
import com.artifex.mupdf.view.ReaderView;
import com.example.retrofitandroid.BaiLarnService;
import com.example.retrofitandroid.DateDeserializer;
import com.example.retrofitandroid.request.SearchDictionaryRequest;
import com.example.retrofitandroid.request.TokenRenewalRequest;
import com.example.retrofitandroid.request.UsageRequest;
import com.example.retrofitandroid.request.UserUsageRequest;
import com.example.retrofitandroid.response.Course;
import com.example.retrofitandroid.response.Material;
import com.example.retrofitandroid.response.Token;
import com.example.retrofitandroid.response.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

public class MaterialViewer extends Activity {
	// Rest Client
	private String api_endpoint;
	private String client_id;
	private String client_secret;
	private RestAdapter restAdapter;
	private BaiLarnService service;
	private Token userToken;
	private String preferences;
	private SharedPreferences sharedPreferences;
	private User userProfile;

	// User Usage Tracking
	private ArrayList<UsageRequest> usageRequestList;
	private Callback<Response> usageTrackingCallback = new Callback<Response>() {
		@Override
		public void failure(RetrofitError err) {
			Toast.makeText(MaterialViewer.this, "Fail", Toast.LENGTH_SHORT)
					.show();
			renewToken();
		}

		@Override
		public void success(Response arg0, Response arg1) {
			Toast.makeText(MaterialViewer.this, "Success", Toast.LENGTH_SHORT)
					.show();
		}
	};

	// === PDF Viewer
	private MuPDFCore core;
	private MuPDFCore cores[];
	private ReaderView docView;
	private MuPDFPageAdapter mDocViewAdapter;
	String[] PDFpaths;
	String pdfPath;
	private Button bookBtn;
	private static final String CURR_PAGE = "Current Page"; // the helper
	Button GoToBtn;
	int end; // last page;
	// PDF Properties
	public static String materialName = "";
	public static String materialId;
	public static String courseTitle = "";
	private int currPage = 1; // default
	// PDF Downloader
	private ProgressDialog pDialog; // Progress Dialog
	public static final int progress_bar_type = 0; // Progress dialog type (0 -
													// for Horizontal progress
													// bar)

	// Helper Class
	private FilesManager filesManager;
	private BookmarkManager bManager;
	SharedPreferences prefs;
	// === Annotation
	private View[] annoViews = new View[4];
	private Button[] buttonMenus = new Button[4];
	private int currentView = -1;
	final int textIndex = 0;
	final int photoIndex = 1;
	final int voiceIndex = 2;
	final int dictIndex = 3;

	EditText textbox;
	TextView timerDisplay;
	ImgHorizontalLayout gallery;
	ListView playlist;
	Button btnPlayer;
	Button btnStopPlayer;
	Button btnRecorder;
	SeekBar audioProgressBar;
	EditText searchbox;

	// Image
	static final int ACTION_TAKE_PHOTO = 100; // action code
	static final int ACTION_IMPORT_DEVICE_PHOTO = 101;
	static final int ACTION_IMPORT_TEXT_APPEND = 200;

	public ArrayList<HashMap<String, String>> imgList = new ArrayList<HashMap<String, String>>();
	// Audio
	private int AUDIO_OUTPUT_FORMAT = MediaRecorder.OutputFormat.THREE_GPP;
	private boolean isRecording = false;// audio recorder status
	private MediaRecorder recorder;
	public ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>(); // audio
																									// list
																									// of
																									// a
																									// current
																									// page
	private MediaPlayer mp;
	private Handler mHandler = new Handler();;
	private Utilities utils;
	private long startTime = 0L;
	long timeInMilliSec = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;

	// methods
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.activity_material_viewer);
			// //////////////////////////////////////////
			// User Usage Tracking
			usageRequestList = new ArrayList<UsageRequest>();
			// //////////////////////////
			// Rest Client Setup
			api_endpoint = getString(R.string.api_endpoint);
			client_id = getString(R.string.client_id);
			client_secret = getString(R.string.client_secret);
			preferences = getString(R.string.preferences);

			// OkHttp Cache Setup
			OkHttpClient okHttpClient = new OkHttpClient();
			File cacheDir = getBaseContext().getCacheDir();
			Cache cache = null;
			try {
				cache = new Cache(cacheDir, 1024);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			okHttpClient.setCache(cache);
			// //////////////////
			Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,
					new DateDeserializer()).create();

			restAdapter = new RestAdapter.Builder()
					.setClient(new OkClient(okHttpClient))
					.setLogLevel(RestAdapter.LogLevel.FULL)
					.setEndpoint(api_endpoint).build();
			service = restAdapter.create(BaiLarnService.class);
			// /////////////////
			// /////////////////
			// Shared Preferences Setup
			sharedPreferences = getSharedPreferences(preferences,
					Context.MODE_PRIVATE);
			// //////////////
			// ////////////////////////////////////////////////////
			// ////////////////////////////////////////////////////
			// ////////////////////////////////////////////////////

			// Check for existing token
			if (sharedPreferences.contains("userToken")) {
				gson = new Gson();
				String json = sharedPreferences.getString("userToken", "");
				userToken = gson.fromJson(json, Token.class);
			}
			// Check for existing userProfile, then display it
			if (sharedPreferences.contains("userProfile")) {
				gson = new Gson();
				String json = sharedPreferences.getString("userProfile", "");
				userProfile = gson.fromJson(json, User.class);
			}
			// //////////////
			// ////////////////////////////////////////////////////
			// ////////////////////////////////////////////////////
			// ////////////////////////////////////////////////////

			Intent intent = getIntent();
			Course course = (Course) intent.getSerializableExtra("course");
			Material mat = (Material) intent.getSerializableExtra("material");
			courseTitle = course.getName();
			materialName = mat.getName();
			materialName = materialName.substring(0, materialName.length() - 4);// remove
																				// .pdf
			materialId = mat.getId();

			filesManager = new FilesManager(getAppName());
			filesManager.deleteOldFiles();
			prefs = PreferenceManager.getDefaultSharedPreferences(this);
			// prefs.edit().clear().commit(); //CLEAR PREF

			// Check for existing waiting request
			if (prefs.contains("userUsageRequest")) {
				gson = new Gson();
				String json = prefs.getString("userUsageRequest", "");
				Toast.makeText(MaterialViewer.this, json, Toast.LENGTH_SHORT)
						.show();
				Type listType = new TypeToken<ArrayList<UserUsageRequest>>() {
				}.getType();
				usageRequestList = new Gson().fromJson(json, listType);
				if (usageRequestList == null) {
					usageRequestList = new ArrayList<UsageRequest>();
				}
			}

			// ==== PDF
			// check if the file exists in the storage
			pdfPath = filesManager.getMatPath() + "/" + materialName + ".pdf";
			File pdf = new File(pdfPath);

			String curr_version = mat.getVersion() + "";
			String save_version = prefs.getString("material_version", "");

			if (pdf.exists() && curr_version.equals(save_version)) { // exist and no
																		// new
																		// version
				displayMaterial(pdfPath);
			} else {
				// if not download it
				if (isNetworkAvailable(getApplicationContext())) {// check internet
					if (!curr_version.equals(save_version)) {
						Toast.makeText(MaterialViewer.this,
								"Downloading the new version", Toast.LENGTH_SHORT)
								.show();
					}
					String api_endpoint = getString(R.string.api_endpoint);
					String file_url = api_endpoint + "material/download?id="
							+ mat.getId();
					/*
					 * Toast.makeText(MaterialViewer.this, file_url,
					 * Toast.LENGTH_LONG).show();
					 */
					new DownloadFileFromURL().execute(file_url);
					prefs.edit().putString("material_version", curr_version)
							.commit();
				} else {// if no internet
					Toast.makeText(MaterialViewer.this, "No Internet Connection",
							Toast.LENGTH_SHORT).show();
				}
			}
			// ==== Annotation
			initializeViews();
			mp = new MediaPlayer();
			utils = new Utilities();
			bManager = new BookmarkManager(getApplicationContext(), materialName);

			refresh();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			renewToken();
		}
	}

	public void onSaveInstanceState(Bundle savedInstanceState) {// device
																// orientation
																// changes
		savedInstanceState.putInt(CURR_PAGE, currPage);
		super.onSaveInstanceState(savedInstanceState);
	}

	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		currPage = savedInstanceState.getInt(CURR_PAGE);
		docView.moveToPage(currPage - 1);
		GoToBtn.setText(currPage + "/" + end);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ACTION_TAKE_PHOTO && resultCode == RESULT_OK) {
			loadGallery(); // reload gallery
		}
		if (requestCode == ACTION_IMPORT_DEVICE_PHOTO
				&& resultCode == RESULT_OK && data != null) {
			Uri imageUri = data.getData();

			Bitmap bitmap = null;
			try {
				bitmap = MediaStore.Images.Media.getBitmap(
						getContentResolver(), imageUri);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			FileOutputStream out = null;
			try {
				File newFile = filesManager.createImageFile();
				out = new FileOutputStream(newFile.getAbsolutePath());
				bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					out.close();
				} catch (Throwable ignore) {
				}
			}

			loadGallery(); // reload gallery
		}
		if (requestCode == ACTION_IMPORT_TEXT_APPEND && resultCode == RESULT_OK
				&& data != null) {
			Uri uri = data.getData();
			String path = uri.getPath();
			File txt = new File(path);
			String result = filesManager.readTextFile(txt);
			String original = textbox.getText().toString();
			String newText = original + result;
			textbox.setText(newText);
		}
	}

	public void renewToken() {
		try {
			String refreshToken = userToken.getRefresh_token();
			TokenRenewalRequest request = new TokenRenewalRequest(refreshToken,
					client_id, client_secret);
			service.renewToken(request, new Callback<Token>() {

				@Override
				public void failure(RetrofitError err) {
					Toast.makeText(
							getApplicationContext(),
							"Token Renewal Failed : " + err.getMessage()
									+ " Please login again", Toast.LENGTH_SHORT)
							.show();

					// logout(null);
				}

				@Override
				public void success(Token token, Response arg1) {
					userToken = token;
					saveToken(token);
					Toast.makeText(getApplicationContext(),
							"Token Renewal Success", Toast.LENGTH_SHORT).show();

				}

			});
		} catch (NullPointerException e) {
			Toast.makeText(getApplicationContext(),
					"Token not found, please login first", Toast.LENGTH_SHORT)
					.show();
		}
	}

	private void saveToken(Token token) {
		Editor prefsEditor = sharedPreferences.edit();
		Gson gson = new Gson();
		String tokenJson = gson.toJson(token);
		prefsEditor.putString("userToken", tokenJson);
		prefsEditor.commit();
	}

	private void saveUsageRequest() {
		Editor prefsEditor = prefs.edit();
		Gson gson = new Gson();
		String json = gson.toJson(usageRequestList);
		prefsEditor.putString("userUsageRequest", json);
		prefsEditor.commit();
		Toast.makeText(MaterialViewer.this,
				"Save" + usageRequestList.toString(), Toast.LENGTH_SHORT)
				.show();
	}

	private void resendUsageRequest() {
		if (isNetworkAvailable(getApplicationContext())) {
			while (!usageRequestList.isEmpty()) {
				if (isNetworkAvailable(getApplicationContext())) {
					UsageRequest usageRequest = usageRequestList.remove(0);
					if (usageRequest instanceof UserUsageRequest) {
						UserUsageRequest request = (UserUsageRequest) usageRequest;
						if (request.getEndpoint() == UserUsageRequest.TAKE_NOTE) {
							service.takeNote(userToken.getAccess_token(),
									request, usageTrackingCallback);
						} else if (request.getEndpoint() == UserUsageRequest.TAKE_PHOTO) {
							service.takePhoto(userToken.getAccess_token(),
									request, usageTrackingCallback);
						} else if (request.getEndpoint() == UserUsageRequest.RECORD_VOICE) {
							service.recordVoice(userToken.getAccess_token(),
									request, usageTrackingCallback);
						}
					} else if (usageRequest instanceof SearchDictionaryRequest) {
						SearchDictionaryRequest request = (SearchDictionaryRequest) usageRequest;
						service.searchDictionary(userToken.getAccess_token(),
								request, usageTrackingCallback);
					}

					saveUsageRequest();
				} else {
					break;
				}
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mp.release();

		// Before going back, resend the waiting usage request
		resendUsageRequest();
	}

	// =============
	public String getAppName() {// help the filesManager
		return getApplicationContext().getString(R.string.app_name);
	}

	public static boolean isNetworkAvailable(Context context) {
		return ((ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getActiveNetworkInfo() != null;
	}

	/**
	 * PDF
	 */
	private void displayMaterial(String path) {
		PDFpaths = new String[1];
		PDFpaths[0] = path;
		cores = new MuPDFCore[PDFpaths.length];
		for (int i = 0; i < PDFpaths.length; i++) {
			cores[i] = openFile(PDFpaths[i]);
		}
		createUI();
	}

	private void createUI() {

		mDocViewAdapter = new MuPDFPageAdapter(this, core);
		LinearLayout layout = (LinearLayout) findViewById(R.id.viewerContainer);
		// Material Title
		TextView mat1 = (TextView) findViewById(R.id.textView2);
		final String matName = materialName;
		mat1.setText(matName);

		TextView mat2 = (TextView) findViewById(R.id.textView3);
		final String couName = courseTitle;
		mat2.setText(couName);

		// GoToPage Button Initiation
		GoToBtn = (Button) findViewById(R.id.gotoPageBtn);
		end = mDocViewAdapter.getCount(); // last page
		GoToBtn.setText(currPage + "/" + end);
		// ---------------------------------------------------------------------------
		// Document Viewer Section
		docView = new DocumentReaderView(this) {
			// *************** THIS METHOD IS ENTERED EVERY TIME A CURR PAGE
			// CHANGED ***************
			@Override
			protected void onMoveToChild(View view, int i) {
				super.onMoveToChild(view, i);
				currPage = getCurrentPage();
				GoToBtn.setText(currPage + "/" + end);
				refresh(); // call Annotation's refresh() every time we change
							// page
			}

			@Override
			public boolean onFling(MotionEvent e1, MotionEvent e2,
					float distanceX, float distanceY) {
				boolean boo = super.onFling(e1, e2, distanceX, distanceY);
				refresh();
				return boo;
			}

			@Override
			protected void onContextMenuClick() {
			}

			@Override
			protected void onBuy(String path) {
			}

		};
		docView.setAdapter(mDocViewAdapter);

		layout.addView(docView);

		// Button Configuration
		// First Page Button
		Button firstBtn = (Button) findViewById(R.id.firstPageBtn);
		firstBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				docView.moveToPage(0);
				currPage = docView.getCurrentPage();
				GoToBtn.setText(currPage + "/" + end);
				refresh();
			}
		});
		// Previous Page Button
		Button prevBtn = (Button) findViewById(R.id.prevPageBtn);
		prevBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				docView.moveToPrevious();
				currPage = docView.getCurrentPage();
				GoToBtn.setText(currPage + "/" + end);
			}
		});
		// Goto Page Button Setting
		GoToBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				AlertDialog.Builder dialog = new AlertDialog.Builder(
						MaterialViewer.this);
				dialog.setTitle("Go to page");
				final EditText input = new EditText(MaterialViewer.this);
				input.setSingleLine();
				input.setInputType(InputType.TYPE_CLASS_NUMBER);
				dialog.setView(input);
				dialog.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int whichButton) {
								String strPage = input.getEditableText()
										.toString();
								int inputPage = 0;
								try {
									inputPage = Integer.parseInt(strPage);
								} catch (NumberFormatException ignore) {
								}
								if ((inputPage >= 1) && (inputPage <= end)) {
									docView.moveToPage(inputPage - 1);
									currPage = docView.getCurrentPage();
									GoToBtn.setText(currPage + "/" + end);
									refresh();
								}
							}
						});
				dialog.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int whichButton) {
								dialog.cancel();
							}
						});
				AlertDialog alertDialog = dialog.create();
				alertDialog.show();
			}
		});
		// Next Page Button
		Button nextBtn = (Button) findViewById(R.id.nextPageBtn);
		nextBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				docView.moveToNext();
				currPage = docView.getCurrentPage();
				GoToBtn.setText(currPage + "/" + end);
			}
		});
		// Last Page Button
		Button lastBtn = (Button) findViewById(R.id.lastPageBtn);
		lastBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				docView.moveToPage(end - 1);
				currPage = docView.getCurrentPage();
				GoToBtn.setText(currPage + "/" + end);
				refresh();
			}
		});
	}

	private MuPDFCore openFile(String path) {
		try {
			core = new MuPDFCore(path);
		} catch (Exception e) {
			Log.e(getString(R.string.app_name), "get core failed", e);
			return null;
		}
		return core;
	}

	/**
	 * Background Async Task to download file
	 * */
	class DownloadFileFromURL extends AsyncTask<String, String, String> {
		// Before starting background thread Show Progress Bar Dialog
		@SuppressWarnings("deprecation")
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showDialog(progress_bar_type);
		}

		// Downloading file in background thread
		@Override
		protected String doInBackground(String... f_url) {
			int count;
			try {
				URL url = new URL(f_url[0]);
				URLConnection conection = url.openConnection();
				conection.connect();
				// this will be useful so that you can show a tipical 0-100%
				// progress bar
				int lenghtOfFile = conection.getContentLength();
				InputStream input = new BufferedInputStream(url.openStream(),
						8192);
				File home = new File(filesManager.getMatPath());
				if (!home.exists()) {
					home.mkdirs();
				}
				String pdfPath = home.getAbsolutePath() + "/" + materialName
						+ ".pdf";
				OutputStream output = new FileOutputStream(pdfPath);
				byte data[] = new byte[1024];
				long total = 0;
				while ((count = input.read(data)) != -1) {
					total += count;
					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress("" + (int) ((total * 100) / lenghtOfFile));
					// writing data to file
					output.write(data, 0, count);
				}
				// flushing output
				output.flush();
				// closing streams
				output.close();
				input.close();
				/*
				 * Toast.makeText(MaterialViewer.this, "Material is downloaded",
				 * Toast.LENGTH_SHORT).show();
				 */

			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
			}

			return null;
		}

		// Updating progress bar
		protected void onProgressUpdate(String... progress) {
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}

		// After completing background task. Dismiss the progress dialog
		@SuppressWarnings("deprecation")
		@Override
		protected void onPostExecute(String file_url) {
			// dismiss the dialog after the file was downloaded
			dismissDialog(progress_bar_type);
			File file = new File(pdfPath);
			if (file.exists()) {
				displayMaterial(pdfPath);
			} else {
				Toast.makeText(MaterialViewer.this,
						"File is not successfully downloaded",
						Toast.LENGTH_SHORT).show();
			}

		}

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case progress_bar_type: // we set this to 0
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Downloading file. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}

	/**
	 * Annotation
	 */
	private void setMainBtnClick(Button btn, View v, View scroll, View zone) {
		if (v.getVisibility() == View.VISIBLE) {// change to hide
			v.setVisibility(View.GONE);
			resetButton(btn);
			zone.setVisibility(View.GONE);
			scroll.setVisibility(View.GONE);
			currentView = -1;
		} else {// show it
			v.setVisibility(View.VISIBLE);
			setButton(btn);
			zone.setVisibility(View.VISIBLE);
			scroll.setVisibility(View.VISIBLE);
		}
	}

	private void resetButton(Button btn) {
		btn.setBackgroundColor(color.white);
	}

	@SuppressLint("NewApi")
	private void setButton(Button btn) {
		Drawable image = getResources().getDrawable(R.drawable.selected_button);
		btn.setBackground(image);
	}

	private void initializeViews() {
		annoViews[textIndex] = findViewById(R.id.textZone);
		annoViews[photoIndex] = findViewById(R.id.photoZone);
		annoViews[voiceIndex] = findViewById(R.id.voiceZone);
		annoViews[dictIndex] = findViewById(R.id.dictZone);
		buttonMenus[textIndex] = (Button) findViewById(R.id.btnText);
		buttonMenus[photoIndex] = (Button) findViewById(R.id.btnPhoto);
		buttonMenus[voiceIndex] = (Button) findViewById(R.id.btnVoice);
		buttonMenus[dictIndex] = (Button) findViewById(R.id.btnDict);
		final View vs = findViewById(R.id.verticalScrollView);
		final View vb = findViewById(R.id.annotationZone);
		// main buttons
		buttonMenus[textIndex].setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				for (int i = 0; i < annoViews.length; i++) {
					View v = annoViews[i];
					if (i != textIndex) {
						v.setVisibility(View.GONE);
						resetButton(buttonMenus[i]);
					} else {
						setMainBtnClick(buttonMenus[textIndex], v, vs, vb);
						currentView = textIndex;

						// Update usage to the server
						if (v.getVisibility() == View.VISIBLE) {
							UserUsageRequest request = new UserUsageRequest(
									userProfile.getId(), materialId);
							if (isNetworkAvailable(getApplicationContext())) {
								resendUsageRequest();
								service.takeNote(userToken.getAccess_token(),
										request, usageTrackingCallback);
							} else {
								// Save the request
								request.setEndpoint(UserUsageRequest.TAKE_NOTE);
								usageRequestList.add(request);
								saveUsageRequest();
							}
						}
					}
				}
			}
		});
		buttonMenus[photoIndex].setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				for (int i = 0; i < annoViews.length; i++) {
					View v = annoViews[i];
					if (i != photoIndex) {
						v.setVisibility(View.GONE);
						resetButton(buttonMenus[i]);
					} else {
						setMainBtnClick(buttonMenus[photoIndex], v, vs, vb);
						currentView = photoIndex;
					}
				}
			}
		});
		buttonMenus[voiceIndex].setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				for (int i = 0; i < annoViews.length; i++) {
					View v = annoViews[i];
					if (i != voiceIndex) {
						v.setVisibility(View.GONE);
						resetButton(buttonMenus[i]);
					} else {
						setMainBtnClick(buttonMenus[voiceIndex], v, vs, vb);
						currentView = voiceIndex;
					}
				}
			}
		});
		buttonMenus[dictIndex].setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				for (int i = 0; i < annoViews.length; i++) {
					View v = annoViews[i];
					if (i != dictIndex) {
						v.setVisibility(View.GONE);
						resetButton(buttonMenus[i]);
					} else {
						setMainBtnClick(buttonMenus[dictIndex], v, vs, vb);
						currentView = dictIndex;
					}
				}
			}
		});

		// inside elements
		audioProgressBar = (SeekBar) findViewById(R.id.seekBar1);
		btnRecorder = (Button) findViewById(R.id.btnRecorder);
		btnPlayer = (Button) findViewById(R.id.btnPlayer);
		textbox = (EditText) findViewById(R.id.textbox);
		playlist = (ListView) findViewById(R.id.playList);
		timerDisplay = (TextView) findViewById(R.id.tvTimer);
		Button btnTakePhoto = (Button) findViewById(R.id.btnTakePhoto);
		gallery = (ImgHorizontalLayout) findViewById(R.id.gallery);
		Button deviceImgImport = (Button) findViewById(R.id.btnImportDevicePhoto);
		searchbox = (EditText) findViewById(R.id.searchbox);
		Button btnSearch = (Button) findViewById(R.id.btnSearchDict);
		btnStopPlayer = (Button) findViewById(R.id.btnStopPlayer);
		Button textImportAppend = (Button) findViewById(R.id.btnImportAppend);
		Button textExport = (Button) findViewById(R.id.btnTextExport);

		btnRecorder.setOnClickListener(handleRecordClick);
		btnPlayer.setOnClickListener(handlePlayerClick);
		btnTakePhoto.setOnClickListener(takePhoto);
		audioProgressBar.setOnSeekBarChangeListener(seekBarListener);
		deviceImgImport.setOnClickListener(importDevicePhoto);
		textImportAppend.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent();
				intent.addCategory(Intent.CATEGORY_OPENABLE);
				intent.setType("text/plain");
				intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(Intent.createChooser(intent,
						"Choose an app to browse a file"),
						ACTION_IMPORT_TEXT_APPEND);
			}
		});
		textExport.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (filesManager.writeTextFile(textbox.getText().toString())) {
					Toast.makeText(MaterialViewer.this, "Export successfully",
							Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(MaterialViewer.this, "Failed to export",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		btnSearch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String theText = searchbox.getText().toString().toLowerCase();

				// Update usage to the server
				SearchDictionaryRequest request = new SearchDictionaryRequest(
						theText);
				if (isNetworkAvailable(getApplicationContext())) {
					resendUsageRequest();
					service.searchDictionary(userToken.getAccess_token(),
							request, usageTrackingCallback);
				} else {
					// Save the request
					usageRequestList.add(request);
					saveUsageRequest();
				}
				// //////////////////

				Intent intent = new Intent(getApplicationContext(),
						DictionaryResult.class);
				intent.putExtra("keyword", theText);
				startActivity(intent);
			}
		});
		btnStopPlayer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				hideAudioPlayer();
			}
		});
		textbox.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {

				String theText = textbox.getText().toString();
				String title = getString(R.string.note);
				if (theText.equals("")) {// empty
					prefs.edit().remove(filesManager.getCurrentPath()).commit();
					buttonMenus[textIndex].setText(title);
				} else {
					prefs.edit()
							.putString(filesManager.getCurrentPath(), theText)
							.commit();
					title = title + "(1)";
					buttonMenus[textIndex].setText(title);
				}
			}
		});

		// Bookmark Button
		bookBtn = (Button) findViewById(R.id.bookmarkPageBtn);
		bookBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean status = bManager.setunset(currPage);
				changeBookmarkUI(status);
			}
		});
	}// ========== Page Handler ===========

	// reload resources for a new page
	private void refresh() {
		// -------------------------------------------------------------------------
		findViewById(R.id.verticalScrollView).setVisibility(View.GONE);
		for (View v : annoViews) {
			v.setVisibility(View.GONE);
		}
		for (Button btn : buttonMenus) {
			resetButton(btn);
		}
		// -------------------------------------------------------------------------
		filesManager.setCurrentPage(currPage); // always call this before
												// getting resources
		loadPlaylist(); // audio
		hideAudioPlayer();
		loadGallery(); // image
		loadNote(); // text
		// checkBookmark
		boolean status = bManager.checkBookmark(currPage);
		changeBookmarkUI(status);
	}

	@SuppressWarnings("deprecation")
	private void changeBookmarkUI(boolean status) {// bookmark appearance of the
													// current page
		if (status == true) {// set
			bookBtn.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.ic_action_important));
		} else {// unset
			bookBtn.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.ic_action_not_important));
		}
		bookBtn.invalidate();
	}

	// text
	private void loadNote() {
		// textbox.setText(filesManager.getText());
		String text = prefs.getString(filesManager.getCurrentPath(), "");
		textbox.setText(text);
		// automatically open note tab if note exists
		String title = getString(R.string.note);
		if (!text.trim().equals("")) {
			title = title + "(1)";
		}
		buttonMenus[textIndex].setText(title);
	}

	// ========== Image Zone =======
	private View.OnClickListener takePhoto = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			if (!filesManager.isExternalStorageWritable()) {
				Toast.makeText(MaterialViewer.this,
						"External storage is not available for write",
						Toast.LENGTH_SHORT).show();
			} else {
				// ******** Camera Intent Start ***************//*
				Intent takePictureIntent = new Intent(
						MediaStore.ACTION_IMAGE_CAPTURE);
				// Ensure that there's a camera activity to handle the intent
				if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
					// Create the File where the photo should go
					File photoFile = null;
					try {
						photoFile = filesManager.createImageFile(); // to save
																	// photo
																	// file
					} catch (IOException ex) {
						// Error occurred while creating the File
						Log.v(getString(R.string.app_name),
								"ERROR in creating image file");
					}
					// Continue only if the File was successfully created
					if (photoFile != null) {
						// Update usage to the server
						UserUsageRequest request = new UserUsageRequest(
								userProfile.getId(), materialId);
						if (isNetworkAvailable(getApplicationContext())) {
							resendUsageRequest();
							service.takePhoto(userToken.getAccess_token(),
									request, usageTrackingCallback);
						} else {
							// Save the request
							request.setEndpoint(UserUsageRequest.TAKE_PHOTO);
							usageRequestList.add(request);
							saveUsageRequest();
						}
						// //////////////////
						takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
								Uri.fromFile(photoFile));
						takePictureIntent.putExtra(
								"android.intent.extras.CAMERA_FACING",
								Camera.CameraInfo.CAMERA_FACING_BACK);
						startActivityForResult(takePictureIntent,
								ACTION_TAKE_PHOTO);
					}
				}
			}
		}
	};

	private void loadGallery() {
		gallery.clear();
		imgList = filesManager.getImgList();
		int size = imgList.size();
		for (int i = 0; i < size; i++) {
			gallery.add(imgList.get(i).get("imgPath"));
		}
		String title = getString(R.string.photo);
		if (size > 0) {
			title = title + "(" + size + ")";
		}
		buttonMenus[photoIndex].setText(title);

	}

	private View.OnClickListener importDevicePhoto = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			/*
			 * Intent pickImageIntent = new Intent(Intent.ACTION_PICK,
			 * android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			 * pickImageIntent.setType("image/*");
			 * startActivityForResult(pickImageIntent,
			 * ACTION_IMPORT_DEVICE_PHOTO);
			 */
			Intent intent = new Intent();
			intent.addCategory(Intent.CATEGORY_OPENABLE);
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(Intent.createChooser(intent,
					"Choose an app to browse a file"),
					ACTION_IMPORT_DEVICE_PHOTO);
		}
	};
	/*
	 * ======================= Audio Zone =================
	 */

	private View.OnClickListener handleRecordClick = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			if (!isRecording) {
				// Update usage to the server
				UserUsageRequest request = new UserUsageRequest(
						userProfile.getId(), materialId);
				if (isNetworkAvailable(getApplicationContext())) {
					resendUsageRequest();
					service.recordVoice(userToken.getAccess_token(), request,
							usageTrackingCallback);
				} else {
					// Save the request
					request.setEndpoint(UserUsageRequest.RECORD_VOICE);
					usageRequestList.add(request);
					saveUsageRequest();
				}
				// //////////////////

				Toast.makeText(MaterialViewer.this, "Start Recording",
						Toast.LENGTH_SHORT).show();
				startRecording();
			} else {
				Toast.makeText(MaterialViewer.this, "Stop Recording",
						Toast.LENGTH_SHORT).show();
				stopRecording();
			}
			// update songsList
			songsList = filesManager.getPlayList();
		}
	};

	private View.OnClickListener handlePlayerClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// check for already playing
			if (mp.isPlaying()) {
				if (mp != null) {
					mp.pause();
					// Changing button image to play button
					// btnPlay.setImageResource(R.drawable.btn_play);
					btnPlayer.setText(R.string.play_player);
					btnPlayer.setCompoundDrawablesWithIntrinsicBounds(
							R.drawable.ic_action_play, 0, 0, 0);
				}
			} else {
				// Resume song
				if (mp != null) {
					mp.start();
					// Changing button image to pause button
					// btnPlay.setImageResource(R.drawable.btn_pause);
					btnPlayer.setText(R.string.pause_player);
					btnPlayer.setCompoundDrawablesWithIntrinsicBounds(
							R.drawable.ic_action_pause, 0, 0, 0);
				}
			}
		}
	};

	// ================= Playlist ================
	private void loadPlaylist() {
		songsList = filesManager.getPlayList();
		String title = getString(R.string.voice);
		int size = songsList.size();
		if (size > 0)
			title = title + "(" + size + ")";
		buttonMenus[voiceIndex].setText(title);
		ListAdapter adapter = new SimpleAdapter(getApplicationContext(),
				songsList, R.layout.playlist_item,
				new String[] { "songTitle" }, new int[] { R.id.songTitle });

		playlist.setAdapter(adapter);
		playlist.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				int songIndex = position;
				playSong(songIndex);
			}
		});
	}

	// ================= Audio Player ==============

	// start media player
	public void playSong(int songIndex) {
		try {
			mp.reset();
			mp.setDataSource(songsList.get(songIndex).get("songPath"));
			mp.prepare();
			mp.start();
			songsList.get(songIndex).get("songTitle");

			showAudioPlayer();
			// lockScreenRotation();
			// set Progress bar values
			audioProgressBar.setProgress(0);
			audioProgressBar.setMax(100);

			// Updating progress bar
			updateProgressBar();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void hideAudioPlayer() {
		mp.stop();
		mp.reset();
		mHandler.removeCallbacks(mUpdateTimeTask);
		audioProgressBar.setVisibility(View.GONE);
		btnPlayer.setVisibility(View.GONE);
		btnStopPlayer.setVisibility(View.GONE);
		btnRecorder.setVisibility(View.VISIBLE);
		// releaseRotationLock();
	}

	private void showAudioPlayer() {
		audioProgressBar.setVisibility(View.VISIBLE);
		btnPlayer.setVisibility(View.VISIBLE);
		btnStopPlayer.setVisibility(View.VISIBLE);
		btnPlayer.setText(R.string.pause_player); // isPlaying so prompt for
													// pause
		btnPlayer.setCompoundDrawablesWithIntrinsicBounds(
				R.drawable.ic_action_pause, 0, 0, 0);
		// not allow to record
		btnRecorder.setVisibility(View.GONE);
	}

	private void lockScreenRotation() {
		// Stop the screen orientation changing during an event
		switch (this.getResources().getConfiguration().orientation) {
		case Configuration.ORIENTATION_PORTRAIT:
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			break;
		case Configuration.ORIENTATION_LANDSCAPE:
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			break;
		}
	}

	private void releaseRotationLock() {
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
	}

	// *** SeekBar Code **
	private SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {
		// When user stops moving the progress
		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			mHandler.removeCallbacks(mUpdateTimeTask);
			int totalDuration = mp.getDuration();
			int currentPosition = utils.progressToTimer(seekBar.getProgress(),
					totalDuration);
			// forward or backward to certain seconds
			mp.seekTo(currentPosition);
			// update timer progress again
			updateProgressBar();
		}

		// When user starts moving the progress handler
		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// remove message Handler from updating progress bar
			mHandler.removeCallbacks(mUpdateTimeTask);
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			// TODO Auto-generated method stub

		}
	};

	// update timer on seekbar
	public void updateProgressBar() {
		mHandler.postDelayed(mUpdateTimeTask, 100);
	}

	// background runnable thread
	private Runnable mUpdateTimeTask = new Runnable() {
		public void run() {
			long totalDuration = mp.getDuration();
			long currentDuration = mp.getCurrentPosition();
			if (currentDuration == totalDuration) { // finish playing
				hideAudioPlayer();
				releaseRotationLock();
			}

			// Displaying Total Duration time
			// songTotalDurationLabel.setText(""+utils.milliSecondsToTimer(totalDuration));
			// Displaying time completed playing
			// songCurrentDurationLabel.setText(""+utils.milliSecondsToTimer(currentDuration));

			// Updating progress bar
			int progress = (int) (utils.getProgressPercentage(currentDuration,
					totalDuration));
			// Log.d("Progress", ""+progress);
			audioProgressBar.setProgress(progress);

			// Running this thread after 100 milliseconds
			mHandler.postDelayed(this, 100);
		}
	};

	// ----- Recorder ----//

	private void startRecording() {
		if (filesManager.isExternalStorageWritable()) {// should check before
														// write
			isRecording = true;
			recorder = new MediaRecorder();

			lockScreenRotation();
			recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			recorder.setOutputFormat(AUDIO_OUTPUT_FORMAT);
			recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			recorder.setOutputFile(filesManager.getAudioFilename());

			recorder.setOnErrorListener(errorListener);
			recorder.setOnInfoListener(infoListener);

			try {
				recorder.prepare();
				recorder.start();
				//
				startTime = SystemClock.uptimeMillis();
				mHandler.postDelayed(updateTimerThread, 0);
				//
				btnRecorder.setText(R.string.stop_record);
				btnRecorder.setCompoundDrawablesWithIntrinsicBounds(
						R.drawable.ic_action_stop, 0, 0, 0);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private void stopRecording() {
		isRecording = false;
		if (null != recorder) {
			try {
				recorder.stop();
				recorder.reset();
				recorder.release();

				recorder = null;
				btnRecorder.setText(R.string.start_record);
				btnRecorder.setCompoundDrawablesWithIntrinsicBounds(
						R.drawable.ic_rec_start, 0, 0, 0);
				// timeSwapBuff += timeInMilliSec;
				timeSwapBuff = 0L;
				timeInMilliSec = 0L;
				updatedTime = 0L;
				mHandler.removeCallbacks(updateTimerThread);
				timerDisplay.setText("");
				// reload the playlist
				loadPlaylist();

			} catch (RuntimeException stopException) {
				// handle cleanup here
				Log.d("Recording Handler", "Runtime Exception");
			}

			releaseRotationLock();
		}
	}

	// time record display
	private Runnable updateTimerThread = new Runnable() {
		public void run() {
			timeInMilliSec = SystemClock.uptimeMillis() - startTime;
			updatedTime = timeSwapBuff + timeInMilliSec;
			int secs = (int) (updatedTime / 1000);
			int mins = secs / 60;
			secs = secs % 60;
			int milliseconds = (int) (updatedTime % 1000);
			timerDisplay.setText("" + mins + ":" + String.format("%02d", secs)
					+ ":" + String.format("%03d", milliseconds));
			mHandler.postDelayed(this, 0);
		}
	};

	private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
		@Override
		public void onError(MediaRecorder mr, int what, int extra) {
			Toast.makeText(MaterialViewer.this,
					"Error: " + what + ", " + extra, Toast.LENGTH_SHORT).show();
		}
	};

	private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
		@Override
		public void onInfo(MediaRecorder mr, int what, int extra) {
			Toast.makeText(MaterialViewer.this,
					"Warning: " + what + ", " + extra, Toast.LENGTH_SHORT)
					.show();
		}
	};

}